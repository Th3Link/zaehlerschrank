EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L elektrik:RCD U?
U 1 1 602017E3
P 850 5750
F 0 "U?" V 475 5750 50  0000 C CNN
F 1 "RCD" V 566 5750 50  0000 C CNN
F 2 "" H 950 6000 50  0001 C CNN
F 3 "" H 950 6000 50  0001 C CNN
	1    850  5750
	0    1    1    0   
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 602036E4
P 1700 7600
F 0 "U?" H 1750 7600 50  0000 R CNN
F 1 "Treppe" H 1850 7500 50  0000 R CNN
F 2 "" H 1700 7600 50  0001 C CNN
F 3 "" H 1700 7600 50  0001 C CNN
	1    1700 7600
	-1   0    0    1   
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 60203ACC
P 2350 7600
F 0 "U?" H 2400 7600 50  0000 R CNN
F 1 "Brauerei" H 2500 7500 50  0000 R CNN
F 2 "" H 2350 7600 50  0001 C CNN
F 3 "" H 2350 7600 50  0001 C CNN
	1    2350 7600
	-1   0    0    1   
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 60203DED
P 3000 7600
F 0 "U?" H 3050 7600 50  0000 R CNN
F 1 "Hobby" H 3100 7500 50  0000 R CNN
F 2 "" H 3000 7600 50  0001 C CNN
F 3 "" H 3000 7600 50  0001 C CNN
	1    3000 7600
	-1   0    0    1   
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 602040CE
P 4950 7600
F 0 "U?" H 5000 7600 50  0000 R CNN
F 1 "Werkstatt" H 5100 7500 50  0000 R CNN
F 2 "" H 4950 7600 50  0001 C CNN
F 3 "" H 4950 7600 50  0001 C CNN
	1    4950 7600
	-1   0    0    1   
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 6020441A
P 8950 6450
F 0 "U?" H 9000 6450 50  0000 R CNN
F 1 "Werkstatt 400V" H 9200 6350 50  0000 R CNN
F 2 "" H 8950 6450 50  0001 C CNN
F 3 "" H 8950 6450 50  0001 C CNN
	1    8950 6450
	-1   0    0    1   
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 6020482A
P 9600 6450
F 0 "U?" H 9650 6450 50  0000 R CNN
F 1 "Heizung 400V" H 9850 6350 50  0000 R CNN
F 2 "" H 9600 6450 50  0001 C CNN
F 3 "" H 9600 6450 50  0001 C CNN
	1    9600 6450
	-1   0    0    1   
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 60204E16
P 3650 7600
F 0 "U?" H 3700 7600 50  0000 R CNN
F 1 "Waschmaschiene" H 3900 7500 50  0000 R CNN
F 2 "" H 3650 7600 50  0001 C CNN
F 3 "" H 3650 7600 50  0001 C CNN
	1    3650 7600
	-1   0    0    1   
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 60206E19
P 4300 7600
F 0 "U?" H 4350 7600 50  0000 R CNN
F 1 "Garage" H 4450 7500 50  0000 R CNN
F 2 "" H 4300 7600 50  0001 C CNN
F 3 "" H 4300 7600 50  0001 C CNN
	1    4300 7600
	-1   0    0    1   
$EndComp
$Comp
L Device:CircuitBreaker_3P CB?
U 1 1 6020E358
P 8950 5100
F 0 "CB?" H 9203 5146 50  0000 L CNN
F 1 "CB" H 9203 5055 50  0000 L CNN
F 2 "" H 9250 5000 50  0001 C CNN
F 3 "~" H 9200 5100 50  0001 C CNN
	1    8950 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:CircuitBreaker_3P CB?
U 1 1 6020F979
P 9600 5100
F 0 "CB?" H 9853 5146 50  0000 L CNN
F 1 "CB" H 9853 5055 50  0000 L CNN
F 2 "" H 9900 5000 50  0001 C CNN
F 3 "~" H 9850 5100 50  0001 C CNN
	1    9600 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 602133E0
P 1700 6250
F 0 "CB?" H 1753 6296 50  0000 L CNN
F 1 "CB" H 1753 6205 50  0000 L CNN
F 2 "" H 1700 6250 50  0001 C CNN
F 3 "~" H 1700 6250 50  0001 C CNN
	1    1700 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 60213CE2
P 3000 6250
F 0 "CB?" H 3053 6296 50  0000 L CNN
F 1 "CB" H 3053 6205 50  0000 L CNN
F 2 "" H 3000 6250 50  0001 C CNN
F 3 "~" H 3000 6250 50  0001 C CNN
	1    3000 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 602140A4
P 4950 6250
F 0 "CB?" H 5003 6296 50  0000 L CNN
F 1 "CB" H 5003 6205 50  0000 L CNN
F 2 "" H 4950 6250 50  0001 C CNN
F 3 "~" H 4950 6250 50  0001 C CNN
	1    4950 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 60214453
P 3650 6250
F 0 "CB?" H 3703 6296 50  0000 L CNN
F 1 "CB" H 3703 6205 50  0000 L CNN
F 2 "" H 3650 6250 50  0001 C CNN
F 3 "~" H 3650 6250 50  0001 C CNN
	1    3650 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 60214A2C
P 4300 6250
F 0 "CB?" H 4353 6296 50  0000 L CNN
F 1 "CB" H 4353 6205 50  0000 L CNN
F 2 "" H 4300 6250 50  0001 C CNN
F 3 "~" H 4300 6250 50  0001 C CNN
	1    4300 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 6550 1700 6950
Wire Wire Line
	3000 6550 3000 6950
Wire Wire Line
	4950 6550 4950 6950
Wire Wire Line
	8750 5400 8750 5800
Wire Wire Line
	8950 5800 8950 5550
Wire Wire Line
	9150 5400 9150 5700
Wire Wire Line
	9600 5400 9600 5800
Wire Wire Line
	9800 5800 9800 5400
Wire Wire Line
	9400 5400 9400 5800
Wire Wire Line
	3650 6550 3650 6950
Wire Wire Line
	1700 5700 1700 5950
Wire Wire Line
	1150 5700 1700 5700
Wire Wire Line
	4300 6550 4300 6950
$Comp
L elektrik:Circuit U?
U 1 1 6025B412
P 8500 2000
F 0 "U?" H 8550 2000 50  0000 R CNN
F 1 "Küche" H 8600 1900 50  0000 R CNN
F 2 "" H 8500 2000 50  0001 C CNN
F 3 "" H 8500 2000 50  0001 C CNN
	1    8500 2000
	1    0    0    -1  
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 6025BB99
P 9200 2000
F 0 "U?" H 9250 2000 50  0000 R CNN
F 1 "Spülmaschiene+Ofen" H 9600 1900 50  0000 R CNN
F 2 "" H 9200 2000 50  0001 C CNN
F 3 "" H 9200 2000 50  0001 C CNN
	1    9200 2000
	1    0    0    -1  
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 6025C350
P 9900 2000
F 0 "U?" H 9950 2000 50  0000 R CNN
F 1 "Herd 400V" H 10100 1900 50  0000 R CNN
F 2 "" H 9900 2000 50  0001 C CNN
F 3 "" H 9900 2000 50  0001 C CNN
	1    9900 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 6027AC1B
P 8500 3300
F 0 "CB?" H 8372 3254 50  0000 R CNN
F 1 "CB" H 8372 3345 50  0000 R CNN
F 2 "" H 8500 3300 50  0001 C CNN
F 3 "~" H 8500 3300 50  0001 C CNN
	1    8500 3300
	-1   0    0    1   
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 6027B535
P 9000 3300
F 0 "CB?" H 8872 3254 50  0000 R CNN
F 1 "CB" H 8872 3345 50  0000 R CNN
F 2 "" H 9000 3300 50  0001 C CNN
F 3 "~" H 9000 3300 50  0001 C CNN
	1    9000 3300
	-1   0    0    1   
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 6027B8A2
P 9200 3300
F 0 "CB?" H 9072 3254 50  0000 R CNN
F 1 "CB" H 9072 3345 50  0000 R CNN
F 2 "" H 9200 3300 50  0001 C CNN
F 3 "~" H 9200 3300 50  0001 C CNN
	1    9200 3300
	-1   0    0    1   
$EndComp
$Comp
L Device:CircuitBreaker_3P CB?
U 1 1 6028EDA1
P 9900 3300
F 0 "CB?" H 9572 3254 50  0000 R CNN
F 1 "CB" H 9572 3345 50  0000 R CNN
F 2 "" H 10200 3200 50  0001 C CNN
F 3 "~" H 10150 3300 50  0001 C CNN
	1    9900 3300
	-1   0    0    1   
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 602A3A8C
P 2350 6250
F 0 "CB?" H 2403 6296 50  0000 L CNN
F 1 "CB" H 2403 6205 50  0000 L CNN
F 2 "" H 2350 6250 50  0001 C CNN
F 3 "~" H 2350 6250 50  0001 C CNN
	1    2350 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 5900 3000 5900
Wire Wire Line
	3000 5900 3000 5950
Wire Wire Line
	1700 5700 3650 5700
Wire Wire Line
	3650 5700 3650 5950
Connection ~ 1700 5700
Wire Wire Line
	4300 5800 4300 5950
Wire Wire Line
	3000 5900 4950 5900
Wire Wire Line
	4950 5900 4950 5950
Connection ~ 3000 5900
$Comp
L elektrik:RCD U?
U 1 1 602AAE6F
P 10850 4550
F 0 "U?" V 10475 4550 50  0000 C CNN
F 1 "RCD" V 10566 4550 50  0000 C CNN
F 2 "" H 10950 4800 50  0001 C CNN
F 3 "" H 10950 4800 50  0001 C CNN
	1    10850 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	10550 4500 9800 4500
Wire Wire Line
	9800 4500 9800 4800
Wire Wire Line
	9600 4600 9600 4800
Wire Wire Line
	9600 4600 10550 4600
Wire Wire Line
	10550 4700 9400 4700
Wire Wire Line
	9400 4700 9400 4800
Wire Wire Line
	9800 4500 9150 4500
Wire Wire Line
	9150 4500 9150 4800
Connection ~ 9800 4500
Wire Wire Line
	9600 4600 8950 4600
Wire Wire Line
	8950 4600 8950 4800
Connection ~ 9600 4600
Wire Wire Line
	9400 4700 8750 4700
Wire Wire Line
	8750 4700 8750 4800
Connection ~ 9400 4700
Wire Wire Line
	8500 3000 8500 2650
Wire Wire Line
	9000 3000 9000 2650
Wire Wire Line
	9200 3000 9200 2650
Wire Wire Line
	9700 2650 9700 3000
Wire Wire Line
	9900 2650 9900 3000
Wire Wire Line
	10100 2650 10100 3000
$Comp
L elektrik:RCD U?
U 1 1 602FCF09
P 10850 3850
F 0 "U?" V 10475 3850 50  0000 C CNN
F 1 "RCD" V 10566 3850 50  0000 C CNN
F 2 "" H 10950 4100 50  0001 C CNN
F 3 "" H 10950 4100 50  0001 C CNN
	1    10850 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	10550 3800 10100 3800
Wire Wire Line
	10100 3800 10100 3600
Wire Wire Line
	9900 3900 9900 3600
Wire Wire Line
	9900 3900 10550 3900
Wire Wire Line
	10550 4000 9700 4000
Wire Wire Line
	9700 4000 9700 3600
Wire Wire Line
	10100 3800 9200 3800
Wire Wire Line
	9200 3800 9200 3600
Connection ~ 10100 3800
Wire Wire Line
	9900 3900 9000 3900
Wire Wire Line
	9000 3900 9000 3600
Connection ~ 9900 3900
Wire Wire Line
	9700 4000 8500 4000
Wire Wire Line
	8500 4000 8500 3600
Connection ~ 9700 4000
$Comp
L elektrik:RCD U?
U 1 1 60321D88
P 850 4750
F 0 "U?" V 475 4750 50  0000 C CNN
F 1 "RCD" V 566 4750 50  0000 C CNN
F 2 "" H 950 5000 50  0001 C CNN
F 3 "" H 950 5000 50  0001 C CNN
	1    850  4750
	0    1    1    0   
$EndComp
Text GLabel 8350 3000 0    50   Output ~ 0
SSR
Wire Wire Line
	8350 3000 8500 3000
Connection ~ 8500 3000
Text GLabel 8300 2650 3    50   Input ~ 0
SSR
Text GLabel 1900 6950 1    50   Input ~ 0
SSR
Text GLabel 2550 6950 1    50   Input ~ 0
SSR
Text GLabel 3200 6950 1    50   Input ~ 0
SSR
Text GLabel 4500 6950 1    50   Input ~ 0
SSR
Text GLabel 5150 6950 1    50   Input ~ 0
SSR
Text GLabel 4750 6950 1    50   Input ~ 0
SSR
Text GLabel 1550 6550 0    50   Output ~ 0
SSR
Wire Wire Line
	1550 6550 1700 6550
Connection ~ 1700 6550
Text GLabel 2150 6550 0    50   Output ~ 0
SSR
Wire Wire Line
	1150 5800 2350 5800
Wire Wire Line
	2150 6550 2350 6550
Wire Wire Line
	2350 6550 2350 6950
Connection ~ 2350 6550
Wire Wire Line
	2350 5950 2350 5800
Connection ~ 2350 5800
Wire Wire Line
	2350 5800 4300 5800
Text GLabel 2800 6550 0    50   Output ~ 0
SSR
Wire Wire Line
	2800 6550 3000 6550
Connection ~ 3000 6550
Text GLabel 4100 6550 0    50   Output ~ 0
SSR
Wire Wire Line
	4100 6550 4300 6550
Connection ~ 4300 6550
Text GLabel 4750 6550 0    50   Output ~ 0
SSR
Wire Wire Line
	4750 6550 4950 6550
Connection ~ 4950 6550
Wire Wire Line
	1150 5600 1300 5600
Wire Wire Line
	1300 5600 1300 7650
Wire Wire Line
	1300 7650 1900 7650
Wire Wire Line
	1900 7650 2550 7650
Connection ~ 1900 7650
Connection ~ 2550 7650
Wire Wire Line
	2550 7650 3200 7650
Connection ~ 3200 7650
Wire Wire Line
	3200 7650 3850 7650
Connection ~ 3850 7650
Wire Wire Line
	3850 7650 4500 7650
Connection ~ 4500 7650
Wire Wire Line
	4500 7650 5150 7650
Wire Wire Line
	10550 4400 10350 4400
Wire Wire Line
	10350 4400 10350 6500
Wire Wire Line
	10350 6500 9800 6500
Connection ~ 9800 6500
Wire Wire Line
	9800 6500 9150 6500
Wire Wire Line
	10550 3700 10550 1950
Wire Wire Line
	10550 1950 9700 1950
Connection ~ 9000 1950
Wire Wire Line
	9000 1950 8300 1950
Connection ~ 9700 1950
Wire Wire Line
	9700 1950 9000 1950
$Comp
L elektrik:Circuit1P U?
U 1 1 603B8594
P 3350 4700
F 0 "U?" H 3050 4700 50  0000 L CNN
F 1 "Licht Keller" V 3150 4850 50  0000 L CNN
F 2 "" H 3350 4700 50  0001 C CNN
F 3 "" H 3350 4700 50  0001 C CNN
	1    3350 4700
	0    1    1    0   
$EndComp
Wire Notes Line
	8200 1850 8200 4150
Wire Notes Line
	8200 4150 10850 4150
Wire Notes Line
	8200 1850 10850 1850
Wire Notes Line
	10850 1850 10850 4150
Wire Notes Line
	850  7750 850  5500
Wire Notes Line
	10900 4250 10900 6600
Text GLabel 4000 4500 2    50   Output ~ 0
SSR
Text GLabel 4700 4750 2    50   Output ~ 0
SSR
Text GLabel 4000 5000 2    50   Output ~ 0
48V
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 603F2D8F
P 2050 4500
F 0 "CB?" V 2240 4500 50  0000 C CNN
F 1 "CB" V 2149 4500 50  0000 C CNN
F 2 "" H 2050 4500 50  0001 C CNN
F 3 "~" H 2050 4500 50  0001 C CNN
	1    2050 4500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2350 4500 2700 4500
Wire Wire Line
	2700 4750 2350 4750
Wire Wire Line
	3400 4350 3400 4500
$Comp
L elektrik:KellerLicht U?
U 1 1 604838A6
P 6150 650
F 0 "U?" H 6728 359 50  0000 L CNN
F 1 "KellerLicht" H 5950 650 50  0000 L CNN
F 2 "" H 6150 650 50  0001 C CNN
F 3 "" H 6150 650 50  0001 C CNN
	1    6150 650 
	1    0    0    -1  
$EndComp
$Comp
L elektrik:Rolläden U?
U 1 1 60485C49
P 9750 600
F 0 "U?" H 10878 159 50  0000 L CNN
F 1 "Rolläden" H 9600 600 50  0000 L CNN
F 2 "" H 9750 600 50  0001 C CNN
F 3 "" H 9750 600 50  0001 C CNN
	1    9750 600 
	1    0    0    -1  
$EndComp
Wire Notes Line
	850  1850 7100 1850
Wire Notes Line
	850  4100 850  1850
Wire Notes Line
	7100 4100 850  4100
Wire Notes Line
	7100 1850 7100 4100
Wire Wire Line
	5900 1950 6600 1950
Connection ~ 5900 1950
Wire Wire Line
	5200 1950 5900 1950
Connection ~ 5200 1950
Wire Wire Line
	4500 1950 5200 1950
Connection ~ 4500 1950
Wire Wire Line
	3800 1950 4500 1950
Connection ~ 3800 1950
Wire Wire Line
	3100 1950 3800 1950
Connection ~ 3100 1950
Wire Wire Line
	2400 1950 3100 1950
Connection ~ 2400 1950
Wire Wire Line
	1700 1950 2400 1950
Connection ~ 1700 1950
Wire Wire Line
	1150 1950 1700 1950
Wire Wire Line
	1150 3700 1150 1950
Text GLabel 2400 2650 3    50   Input ~ 0
SSR
Text GLabel 3100 2650 3    50   Input ~ 0
SSR
Text GLabel 3800 2650 3    50   Input ~ 0
SSR
Text GLabel 4500 2650 3    50   Input ~ 0
SSR
Text GLabel 6300 2650 3    50   Input ~ 0
SSR
Text GLabel 5900 2650 3    50   Input ~ 0
SSR
Text GLabel 5200 2650 3    50   Input ~ 0
SSR
Wire Wire Line
	3300 2950 3300 3000
Connection ~ 3300 2950
Wire Wire Line
	3200 2950 3300 2950
Text GLabel 3200 2950 0    50   Output ~ 0
SSR
Wire Wire Line
	3300 2650 3300 2950
Wire Wire Line
	3850 2950 4000 2950
Text GLabel 3850 2950 0    50   Output ~ 0
SSR
Wire Wire Line
	5200 3000 5400 3000
Text GLabel 5200 3000 0    50   Output ~ 0
SSR
Connection ~ 4000 2950
Wire Wire Line
	4700 2950 4700 2650
Wire Wire Line
	4000 2950 4700 2950
Connection ~ 6100 3000
Wire Wire Line
	6800 3000 6800 2650
Wire Wire Line
	6100 3000 6800 3000
Wire Wire Line
	6100 3000 6100 2650
Wire Wire Line
	5400 3000 6100 3000
Connection ~ 5400 3000
Connection ~ 2600 4000
Wire Wire Line
	5400 4000 5400 3600
Wire Wire Line
	2600 4000 5400 4000
Connection ~ 1900 3900
Wire Wire Line
	4000 3900 4000 3550
Wire Wire Line
	1900 3900 4000 3900
Connection ~ 1700 3800
Wire Wire Line
	3300 3800 3300 3600
Wire Wire Line
	1700 3800 3300 3800
Wire Wire Line
	2600 4000 2600 3600
Wire Wire Line
	1150 4000 2600 4000
Wire Wire Line
	1900 3900 1900 3600
Wire Wire Line
	1150 3900 1900 3900
Wire Wire Line
	1700 3800 1700 3600
Wire Wire Line
	1150 3800 1700 3800
Wire Wire Line
	4000 2950 4000 2650
Wire Wire Line
	5400 3000 5400 2650
Wire Wire Line
	2600 2650 2600 3000
Wire Wire Line
	1900 2650 1900 3000
Wire Wire Line
	1700 2650 1700 3000
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 6029BAFD
P 1700 3300
F 0 "CB?" H 1572 3254 50  0000 R CNN
F 1 "CB" H 1572 3345 50  0000 R CNN
F 2 "" H 1700 3300 50  0001 C CNN
F 3 "~" H 1700 3300 50  0001 C CNN
	1    1700 3300
	-1   0    0    1   
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 6027DE98
P 5400 3300
F 0 "CB?" H 5273 3254 50  0000 R CNN
F 1 "CircuitBreaker_1P" H 5273 3345 50  0000 R CNN
F 2 "" H 5400 3300 50  0001 C CNN
F 3 "~" H 5400 3300 50  0001 C CNN
	1    5400 3300
	-1   0    0    1   
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 6027BFF6
P 4000 3250
F 0 "CB?" H 3872 3204 50  0000 R CNN
F 1 "CB" H 3872 3295 50  0000 R CNN
F 2 "" H 4000 3250 50  0001 C CNN
F 3 "~" H 4000 3250 50  0001 C CNN
	1    4000 3250
	-1   0    0    1   
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 6027A7E5
P 3300 3300
F 0 "CB?" H 3172 3254 50  0000 R CNN
F 1 "CB" H 3172 3345 50  0000 R CNN
F 2 "" H 3300 3300 50  0001 C CNN
F 3 "~" H 3300 3300 50  0001 C CNN
	1    3300 3300
	-1   0    0    1   
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 6027A559
P 2600 3300
F 0 "CB?" H 2472 3254 50  0000 R CNN
F 1 "CB" H 2472 3345 50  0000 R CNN
F 2 "" H 2600 3300 50  0001 C CNN
F 3 "~" H 2600 3300 50  0001 C CNN
	1    2600 3300
	-1   0    0    1   
$EndComp
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 60278394
P 1900 3300
F 0 "CB?" H 1772 3254 50  0000 R CNN
F 1 "CB" H 1772 3345 50  0000 R CNN
F 2 "" H 1900 3300 50  0001 C CNN
F 3 "~" H 1900 3300 50  0001 C CNN
	1    1900 3300
	-1   0    0    1   
$EndComp
$Comp
L elektrik:RCD U?
U 1 1 60275A7D
P 850 3850
F 0 "U?" V 475 3850 50  0000 C CNN
F 1 "RCD" V 566 3850 50  0000 C CNN
F 2 "" H 950 4100 50  0001 C CNN
F 3 "" H 950 4100 50  0001 C CNN
	1    850  3850
	0    1    1    0   
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 602671E9
P 6800 2000
F 0 "U?" H 6850 2000 50  0000 R CNN
F 1 "Kind West" H 7000 1900 50  0000 R CNN
F 2 "" H 6800 2000 50  0001 C CNN
F 3 "" H 6800 2000 50  0001 C CNN
	1    6800 2000
	1    0    0    -1  
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 60266D4F
P 6100 2000
F 0 "U?" H 6150 2000 50  0000 R CNN
F 1 "Kind Mitte" H 6300 1900 50  0000 R CNN
F 2 "" H 6100 2000 50  0001 C CNN
F 3 "" H 6100 2000 50  0001 C CNN
	1    6100 2000
	1    0    0    -1  
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 6026619E
P 5400 2000
F 0 "U?" H 5450 2000 50  0000 R CNN
F 1 "Kind Ost" H 5550 1900 50  0000 R CNN
F 2 "" H 5400 2000 50  0001 C CNN
F 3 "" H 5400 2000 50  0001 C CNN
	1    5400 2000
	1    0    0    -1  
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 60261A66
P 4700 2000
F 0 "U?" H 4750 2000 50  0000 R CNN
F 1 "Flur" H 4750 1900 50  0000 R CNN
F 2 "" H 4700 2000 50  0001 C CNN
F 3 "" H 4700 2000 50  0001 C CNN
	1    4700 2000
	1    0    0    -1  
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 60260D6D
P 4000 2000
F 0 "U?" H 4050 2000 50  0000 R CNN
F 1 "Schlafzimmer" H 4250 1900 50  0000 R CNN
F 2 "" H 4000 2000 50  0001 C CNN
F 3 "" H 4000 2000 50  0001 C CNN
	1    4000 2000
	1    0    0    -1  
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 6025A64B
P 3300 2000
F 0 "U?" H 3350 2000 50  0000 R CNN
F 1 "Wohnzimmer" H 3550 1900 50  0000 R CNN
F 2 "" H 3300 2000 50  0001 C CNN
F 3 "" H 3300 2000 50  0001 C CNN
	1    3300 2000
	1    0    0    -1  
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 60259B31
P 2600 2000
F 0 "U?" H 2650 2000 50  0000 R CNN
F 1 "Bäder" H 2700 1900 50  0000 R CNN
F 2 "" H 2600 2000 50  0001 C CNN
F 3 "" H 2600 2000 50  0001 C CNN
	1    2600 2000
	1    0    0    -1  
$EndComp
$Comp
L elektrik:Circuit U?
U 1 1 6025848E
P 1900 2000
F 0 "U?" H 1950 2000 50  0000 R CNN
F 1 "Lüftung+Frischwasser" H 2300 1900 50  0000 R CNN
F 2 "" H 1900 2000 50  0001 C CNN
F 3 "" H 1900 2000 50  0001 C CNN
	1    1900 2000
	1    0    0    -1  
$EndComp
Text Notes 550  1050 0    118  ~ 0
L1: braun schaltbar\nL2: schwarz dauerspannung\nL3: grau/weiss schaltbar
Text GLabel 4000 5100 2    50   Output ~ 0
12V
Text GLabel 4000 5200 2    50   Output ~ 0
5V
Text GLabel 5700 1400 3    50   Input ~ 0
SSR
Text GLabel 5850 1400 3    50   Input ~ 0
SSR
Text GLabel 6000 1400 3    50   Input ~ 0
SSR
Text GLabel 6150 1400 3    50   Input ~ 0
SSR
Text GLabel 6300 1400 3    50   Input ~ 0
SSR
Text GLabel 6450 1400 3    50   Input ~ 0
SSR
Text GLabel 6600 1400 3    50   Input ~ 0
SSR
Text GLabel 8700 1550 3    50   Input ~ 0
SSR
Text GLabel 8850 1550 3    50   Input ~ 0
SSR
Text GLabel 9000 1550 3    50   Input ~ 0
SSR
Text GLabel 9150 1550 3    50   Input ~ 0
SSR
Text GLabel 9300 1550 3    50   Input ~ 0
SSR
Text GLabel 9450 1550 3    50   Input ~ 0
SSR
Text GLabel 9600 1550 3    50   Input ~ 0
SSR
Text GLabel 9750 1550 3    50   Input ~ 0
SSR
Text GLabel 9900 1550 3    50   Input ~ 0
SSR
Text GLabel 10050 1550 3    50   Input ~ 0
SSR
Text GLabel 10200 1550 3    50   Input ~ 0
SSR
Text GLabel 10350 1550 3    50   Input ~ 0
SSR
Text GLabel 10500 1550 3    50   Input ~ 0
SSR
Text GLabel 10650 1550 3    50   Input ~ 0
SSR
Text GLabel 10800 1550 3    50   Input ~ 0
SSR
Text Notes 5650 800  0    50   ~ 0
7x PTI 2,5-PE/L/NT
Text Notes 8650 900  0    50   ~ 0
15x PTI 2,5-PE/L/NT\n15x PTI 2,5-L/L
Text Notes 3500 5350 0    50   ~ 0
1x PTI 2,5-PE/L/NT
Text Notes 550  1500 0    118  ~ 0
Blöcke oben = Abgang nach obon\nBlöcke unten = Abgang nach unten
Text Notes 6150 4050 0    50   ~ 0
8x PTI 2,5-PE/L/NT\n8x PTI 2,5-L/L
Text Notes 5050 5750 0    50   ~ 0
6x PTI 2,5-PE/L/NT\n6x PTI 2,5-L/L
Text Notes 8000 4450 0    50   ~ 0
3x PTI 2,5-PE/L/NT\n3x PTI 2,5-L/L
$Comp
L elektrik:Circuit U?
U 1 1 60573E1A
P 8300 6450
F 0 "U?" H 8350 6450 50  0000 R CNN
F 1 "Werkstatt 400V" H 8550 6350 50  0000 R CNN
F 2 "" H 8300 6450 50  0001 C CNN
F 3 "" H 8300 6450 50  0001 C CNN
	1    8300 6450
	-1   0    0    1   
$EndComp
Wire Notes Line
	7950 6600 7950 4250
Wire Notes Line
	7950 4250 10900 4250
Wire Notes Line
	7950 6600 10900 6600
Wire Wire Line
	8500 6500 9150 6500
Connection ~ 9150 6500
Wire Wire Line
	8750 5400 8100 5400
Wire Wire Line
	8100 5400 8100 5800
Connection ~ 8750 5400
Wire Wire Line
	8300 5800 8300 5550
Wire Wire Line
	8300 5550 8950 5550
Connection ~ 8950 5550
Wire Wire Line
	8950 5550 8950 5400
Wire Wire Line
	9150 5700 8500 5700
Wire Wire Line
	8500 5700 8500 5800
Connection ~ 9150 5700
Wire Wire Line
	9150 5700 9150 5800
Text Notes 8250 4100 0    50   ~ 0
3x PTI 2,5-PE/L/NT\n3x PTI 2,5-L/L
Wire Wire Line
	1150 4350 1150 4600
Wire Wire Line
	1150 4350 3400 4350
Wire Wire Line
	1150 4700 1400 4700
Wire Wire Line
	1400 4700 1400 4500
Wire Wire Line
	1400 4500 1750 4500
Wire Wire Line
	1600 4800 1600 4750
Wire Wire Line
	1600 4750 1750 4750
Wire Wire Line
	1150 4800 1600 4800
Wire Notes Line
	850  4200 850  5400
Wire Notes Line
	5800 4200 5800 5400
Wire Notes Line
	850  5400 5800 5400
Wire Notes Line
	850  4200 5800 4200
Wire Wire Line
	3400 5000 3400 5250
$Comp
L elektrik:Circuit1P U?
U 1 1 603C6801
P 3350 5450
F 0 "U?" H 3050 5450 50  0000 L CNN
F 1 "19\" Rack" V 3150 5600 50  0000 L CNN
F 2 "" H 3350 5450 50  0001 C CNN
F 3 "" H 3350 5450 50  0001 C CNN
	1    3350 5450
	0    1    1    0   
$EndComp
$Comp
L elektrik:Circuit1P U?
U 1 1 603C1691
P 3350 5200
F 0 "U?" H 3050 5200 50  0000 L CNN
F 1 "Licht Oben" V 3150 5350 50  0000 L CNN
F 2 "" H 3350 5200 50  0001 C CNN
F 3 "" H 3350 5200 50  0001 C CNN
	1    3350 5200
	0    1    1    0   
$EndComp
Connection ~ 3400 4500
Wire Wire Line
	3400 4500 3400 4750
Connection ~ 3400 5000
Connection ~ 3400 4750
Wire Wire Line
	3400 4750 3400 5000
$Comp
L elektrik:Circuit1P U?
U 1 1 603C0865
P 3350 4950
F 0 "U?" H 3050 4950 50  0000 L CNN
F 1 "Rolläden/Heizkeisverteiler" V 3150 5100 50  0000 L CNN
F 2 "" H 3350 4950 50  0001 C CNN
F 3 "" H 3350 4950 50  0001 C CNN
	1    3350 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	2350 5250 2700 5250
Wire Wire Line
	2350 5000 2700 5000
Wire Wire Line
	2350 5250 2350 5000
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 603F3583
P 2050 4750
F 0 "CB?" V 2240 4750 50  0000 C CNN
F 1 "CB" V 2100 4750 50  0000 C CNN
F 2 "" H 2050 4750 50  0001 C CNN
F 3 "~" H 2050 4750 50  0001 C CNN
	1    2050 4750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 4900 1650 5250
Wire Wire Line
	1150 4900 1650 4900
Wire Wire Line
	1650 5250 1750 5250
Connection ~ 2350 5250
$Comp
L Device:CircuitBreaker_1P CB?
U 1 1 603F2763
P 2050 5250
F 0 "CB?" H 1923 5204 50  0000 R CNN
F 1 "CB" V 2100 5300 50  0000 R CNN
F 2 "" H 2050 5250 50  0001 C CNN
F 3 "~" H 2050 5250 50  0001 C CNN
	1    2050 5250
	0    -1   -1   0   
$EndComp
Text Notes 4750 4400 0    50   ~ 0
Anschluss ans 19" Rack\nDie restlichen CBs intern
$Comp
L elektrik:Circuit U?
U 1 1 6066E311
P 5600 7600
F 0 "U?" H 5650 7600 50  0000 R CNN
F 1 "Werkstatt" H 5750 7500 50  0000 R CNN
F 2 "" H 5600 7600 50  0001 C CNN
F 3 "" H 5600 7600 50  0001 C CNN
	1    5600 7600
	-1   0    0    1   
$EndComp
Text GLabel 5400 6950 1    50   Input ~ 0
SSR
Text GLabel 5800 6950 1    50   Input ~ 0
SSR
Wire Wire Line
	4950 6550 5600 6550
Wire Wire Line
	5600 6550 5600 6950
Wire Wire Line
	5150 7650 5800 7650
Connection ~ 5150 7650
Wire Notes Line
	5900 7750 5900 5500
Wire Notes Line
	850  5500 5900 5500
Wire Notes Line
	850  7750 5900 7750
$EndSCHEMATC
